/* Functions */
ReverseWord=x=>x.split('').reverse().join('');
ReverseWords=x=>x.split(' ').reverse().join(' ');
Min=x=>x.sort((a,b)=>a-b)[0];
Max=x=>x.sort((a,b)=>b-a)[0];
Rem=(a,b)=>a%b;
Distinct=x=>x.filter((v, i, a) => a.indexOf(v) === i);
DistinctwLength=x=>x.filter((v, i, a) => a.indexOf(v) === i).map(value=>value + ` (${x.length - x.filter(u=>u!=value).length})`);
EvaluateExpression=(expression, object)=>{
    let i = 0;
    let sum = 0;
    expression = expression.split('').filter(x=>x=='+'||x=='-');
    expression.unshift('+')
    for (let x in object) {
        if (expression[i] == '+')
            sum+= object[x];
        else
            sum-= object[x];
        i++;
    }
    return sum;
}

/* Test Cases */
//ReverseWord Assertion Tests:
AssertEquals(ReverseWord("word"), "drow");
AssertEquals(ReverseWord("Lesson"), "nosseL");
//ReverseWords Assertion Tests:
AssertEquals(ReverseWords("this is a sentence"), "sentence a is this");
AssertEquals(ReverseWords("Lesson"), "Lesson");
//Min Assertion Tests:
AssertEquals(Min([10,4,33993,2,98]), 2);
AssertEquals(Min([22,888,444,90]), 22);
//Max Assertion Tests:
AssertEquals(Max([10,4,33993,2,98]), 33993);
AssertEquals(Max([22,888,444,90]), 888);
//Rem Assertion Tests:
AssertEquals(Rem(9,3), 0);
AssertEquals(Rem(33,9), 6);
//Distinct Assertion Tests:
AssertEquals(Distinct([1, 3, 5, 3, 7, 3, 1, 1, 5]), [1, 3, 5, 7]);
AssertEquals(Distinct(['word', 'like', '88', 9, '88', 9, false, 'word', false, 9]), ['word', 'like', '88', 9, false]);
//DistinctwLength Assertion Tests:
AssertEquals(DistinctwLength([1, 3, 5, 3, 7, 3, 1, 1, 5]), ['1 (3)', "3 (3)", "5 (2)", '7 (1)']);
AssertEquals(DistinctwLength(['word', 'like', '88', 9, '88', 9, false, 'word', false, 9]), ["word (2)", "like (1)", "88 (2)", "9 (3)", "false (2)"]);
//EvaluateExpression Assertion Tests:
AssertEquals(EvaluateExpression("a + b + c - d", {a: 1, b: 7, c: 3, d: 14}), -3);
AssertEquals(EvaluateExpression("a - b + c - d + e", {a: 10, b: 3, c: 15, d: 2, e: 3}), 23);




function AssertEquals (testCase, expected) {
    if (typeof(expected) == 'object')
        console.assert(testCase.length === expected.length && testCase.sort().every(function(v, i) { return v === expected.sort()[i]}), `Expected: ${expected}, instead got: ${testCase}`);
    else
        console.assert(testCase == expected, `Expected: ${expected}, instead got: ${testCase}`);
}